Shuttercraft Somerset provide a professional supply and fit service for interior shutters and blinds in Bath, We are looking forward to helping you transform your home with premium shutters from our range like our hardwood wooden shutters, MDF shutters, ABS 100% waterproof shutters.

Address: The Guild Hub, High Street, Bath, Somerset BA1 5EB, UK

Phone: +44 1225 459 389

Website: https://www.shuttercraft-somerset.co.uk
